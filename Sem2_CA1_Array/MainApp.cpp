#include "OrderedArray.h"
#include "Menu.h"
#include<iostream>
#include<string>

#define _CRTDBG_MAP_ALLOC

#include<stdlib.h>
#include<crtdbg.h>

using std::cout;
using std::cin;
using std::endl;
using std::string;

int main()
{
	#if defined(DEBUG) | defined(_DEBUG)
		_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);
	#endif

	OrderedArray<int> intArray(10);
	OrderedArray<float> floatArray(10);
	OrderedArray<string> stringArray(10);

	intArray.push(1);
	intArray.push(8);
	intArray.push(12);
	intArray.push(2);
	intArray.push(3);
	intArray.push(43);
	intArray.push(22);
	intArray.push(0);
	intArray.push(21);
	intArray.push(30);
	intArray.clear();
	intArray.ReadUnorderedFile("unordered.txt");
	intArray.print();

	floatArray.push(3.3f);
	floatArray.push(9.3f);
	floatArray.push(7.4f);
	floatArray.push(2.8f);
	floatArray.push(1.5f);

	stringArray.push("Audreen");
	stringArray.push("Stevie");
	stringArray.push("Anna");
	stringArray.push("Matti");

	bool menu=false;
		
	
	do
	{
		menu=showMainMenu(intArray,floatArray,stringArray);
	} while (menu == false); 
		




	cin.ignore();
	return 0;
}


////intArray.print();
///*intArray.push(3);
//intArray.push(1);
//intArray.push(8);
//intArray.push(12);
//intArray.push(2);
//intArray.push(3);
//intArray.push(43);
//intArray.push(22);
//intArray.push(0);
//intArray.push(21);*/
////
////intArray.print();

//intArray.push(3);

////intArray.pop();
////intArray.print();

////intArray.Remove(0);
////intArray.print();

//////intArray.clear();
////intArray.print();
//cout << "reading file" << endl;
//intArray.ReadUnorderedFile("unordered.txt");
//intArray.print();

//int position = intArray.linearSearch(5);
//cout << "Position : " << position << endl;
//int position2=intArray.binarySearch(5);
//cout << "Position : "<<position2 << endl;

//position = intArray.linearSearch(3);
//cout << "Position : " << position << endl;
//position2 = intArray.binarySearch(3);
//cout << "Position : " << position2 << endl;

//intArray.WriteOrderedFile("ordered.txt");


/*floatArray.ReadUnorderedFile("unordered.txt");
floatArray.print();
floatArray.WriteOrderedFile("ordered.txt");*/
