#ifndef ORDEREDARRAY_H
#define ORDEREDARRAY_H

#include <iostream>
#include <fstream>
#include <string>

template <class Datatype>
class OrderedArray
{
private:
	Datatype* m_array;
	int m_size;
	int m_growSize;
	int m_numElements;
public:
	//default constructor
	//set the array size to default =10;
	OrderedArray() :m_array(new Datatype[10]),m_size(10), m_growSize(10), m_numElements(0)
	{

	}

	//Constructor
	//which allows the user to enter ther size
	OrderedArray(int p_size)
	{
		//allocate enough memory for the array
		m_array = new Datatype[p_size];
		m_size = p_size;
	}

	//deconstructor
	~OrderedArray()
	{
		if (m_array != 0)
		{
			//delete everything inside the array
			delete[]m_array;	
			m_array = 0;
		}
		
	}

	//the parameter to resize tells the size of the new array
	void resize(int p_size)
	{
		//step one: Create new array
		Datatype* newArray = 0;
		m_growSize = p_size;
		newArray = new Datatype[p_size];

		//if the new array wasn't allocated, then just return
		//when it fails return 0 or bad_alloc exception
		if (newArray == 0)
		{
			std::cout << "The call to new failed and returned 0" << std::endl;
			return;
		}

		//determine if i am making the new array bigger or smaller
		int min;
		if (m_growSize < m_size)
		{
			min = m_growSize;
		}
		else
		{
			min = m_size;
		}

		//loop through and copy all elements
		for (int index = 0; index < min; index++)
		{
			newArray[index] = m_array[index];
		}

		//set the size of the new array
		m_size = m_growSize;

		//delete the old array
		if (m_array != 0)
		{
			//make sure you use delete[] not just delete
			delete[]m_array;
		}

		//copy the pointer over
		m_array = newArray;
		newArray = 0;
		std::cout << "Array resized" << std::endl;

		
	}

	void setGrowSize(int p_growSize)
	{
		m_growSize = p_growSize;
		std::cout << "growSize changed to " << p_growSize << endl;
	}

	//enter a value to search the index from the array
	int linearSearch(Datatype p_value)
	{
		//go through the array from the beginning till if finds p_value and return the index
		for (int i = 0; i != m_numElements; i++)
		{
			if (m_array[i] == p_value)
			{
				return i;
			}
		}
	}

	//Reference: http://www.cppforschool.com/assignment/array-1d-sol/binary-search.html
	int binarySearch(Datatype p_value)
	{
		int Mid, Lbound = 0, Ubound = m_numElements - 1;

		while (Lbound <= Ubound)
		{
			//get the middle element from the whole array 
			Mid = (Lbound + Ubound) / 2;
			//if p_value is bigger than the middle element value then set the lower bound to the middle element
			if (p_value>m_array[Mid])
				Lbound = Mid + 1;
			//if p_Value is smaller than the middle element value then set the upper bound to the middle element
			else if (p_value<m_array[Mid])
				Ubound = Mid - 1;
			//if p_value is equals to the middle element value then return the index of that value in that array.
			else
				return Mid;
		}

		return 0;
	}

	//override [] so that it works as you would expect with an array
	Datatype& operator[](int p_index)
	{
		return m_array[p_index];
	}

	//conversion operator to convert an array into operator
	//so that we can pass our array into function
	operator Datatype*()
	{
		return m_array;
	}

	//to push the item into the array and sort them while pushing them in
	//if array size is too small, resize it by adding the 
	void push(Datatype p_item)
	{
		if (m_numElements == 0)
		{
			m_array[0] = p_item;
			m_numElements++;
		}
		//to check if the array is full
		else if (m_numElements != m_size)
		{
			for (int i = m_numElements; i >0; i--)
			{
				if (m_array[i-1] < p_item)
				{
					m_array[i] = p_item;
					break;
				}
				else
				{
					m_array[i] = m_array[i-1];
					if (i == 1)
					{
						m_array[0] = p_item;
					}
				}
				
			}m_numElements++;
			std::cout << "Element was added to the array." << std::endl;
		}
		else
		{
			//std::cout << "Array is full" << std::endl;
			//resize to double the size of array
			int newSize = m_size + m_growSize;
			resize(newSize);

			//push the item into new resized aray
			push(p_item);
			//std::cout << "Array resized" << std::endl;
			
		}
	}

	void pop()
	{
		//if its not an empty array, then remove the last element
		if (m_numElements != 0)
		{
			m_numElements--;
			std::cout << "Last element is removed." << std::endl;
		}
		else
		{
			std::cout << "Array is empty." << std::endl;
		}
		//
	}

	//remove - this remove function keeps the order of the array 
	//if order is not important there are better options
	//remove that cares about order has complexity O(n)
	void Remove(int p_index)
	{
		int index;
		
		if (p_index<=0 && p_index<m_size)
		{
			//move everithing after index down one
			for (index = p_index+1; index < m_size; index++)
			{
				m_array[index - 1] = m_array[index];
			}
			m_numElements--;
			cout << "Element at index "<< p_index << " remove success" << endl;
		}
		else
		{
			std::cout << "Invalid index entered. Please try again!" << std::endl;
		}
			
		
		

	}

	//getter to the size of the array
	int Size()
	{
		return m_size;
	}

	//to clear the array
	void clear()
	{
		for (int i = 0; i != m_size; i++)
		{
			m_array[i] = 0;
		}
		m_numElements = 0;
		std::cout << "Array cleared." << std::endl;
	}

	//string array doesnt take 0, so have to create another function to clear string array
	void clearString()
	{
		for (int i = 0; i != m_size; i++)
		{
			m_array[i] = "";
		}
		m_numElements = 0;
		std::cout << "Array cleared." << std::endl;
	}

	//to read in unordered file
	//Push elements while sorting them into array
	bool ReadUnorderedFile(std::string p_filename)
	{
		Datatype number;
		int numberOfElements=0;
		std::fstream infile;
		infile.open(p_filename);
		if (infile.is_open())
		{
			//infile >> numberOfElements;
			//cout << numberOfElements << endl;
			/*for (int i = 0; i < numberOfElements; i++)
			{
				infile >> number;
				cout << number << endl;
				push(number);
				print();
			}*/
			
			while (!infile.eof())
			{
				infile >> number;
				this->push(number);
				//print();
			}
			std::cout << "File read success" << std::endl;
		}
		else
		{
			std::cout << "Unable to open file." << std::endl;
			return false;
		}
		
		infile.close();
		
		return true;
	}

	//write ordered array to file
	bool WriteOrderedFile(std::string p_filename)
	{
		std::ofstream output;
		output.open(p_filename);

		//if file cant be open, maybe it doesnt exists.
		if (!output)
		{
			cout << "file could not be open for writing ! " << endl;
			return false;

		}
		for (int i = 0; i < m_numElements; i++)
		{
			cout << "writting to file" << endl;
			output << m_array[i] << endl;
			cout << m_array[i] << endl;
		}
		std::cout << "File write success" << std::endl;
		output.close();
		return true;
	}

	//Print function to print out the elements in the array
	void print()
	{
		if (m_numElements == 0)
		{
			std::cout << "The array is empty" << std::endl;
			
		}
		for (int i = 0; i < m_numElements; i++)
		{
			std::cout << m_array[i] << "^";
			
		}
		std::cout << std::endl;
	}
};

#endif