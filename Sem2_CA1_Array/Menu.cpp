#include"Menu.h"
#include"timer.h"

using std::cout;
using std::cin;
using std::endl;
using std::string;

//Show Main menu
bool showMainMenu(OrderedArray<int>& intArray, OrderedArray<float>& floatArray, OrderedArray<string>& stringArray)
{ 
	bool exit = false;
	system("CLS");
	cout << "Main Menu:" << endl;
	cout << "1. Manage array" << endl;
	cout << "2. Exit" << endl;

	int choice;
	cin >> choice;
	if (choice == 1)
	{
		//if the user chooses exit in the next menu then it will return equals true and exits the command prompt
		bool datatype = false;
		datatype = chooseArrayDatatype(intArray, floatArray, stringArray);
		if (datatype = true)
		{
			return true;
		}
	}
	else if (choice == 2)
	{
		exit = true;
	}
	cin.clear();
	return exit;
}

//Menu to choose the array datatype
bool chooseArrayDatatype(OrderedArray<int>& intArray, OrderedArray<float>& floatArray, OrderedArray<string>& stringArray)
{
		bool exit = false;
		int secondChoice;
		do{
			system("CLS");
			cout << "1. Int Array" << endl;
			cout << "2. Float Array" << endl;
			cout << "3. String Array" << endl;
			cout << "4. Exit" << endl;

			cin >> secondChoice;
			if (secondChoice == 1)
			{
				//integer functions 
				bool intFunction = chooseIntArrayFunctions(intArray);
				//if user choose to go back in the integer menu, 
				//it will bring them back to this menu
				if (intFunction == true)
				{
					chooseArrayDatatype(intArray, floatArray, stringArray);
				}
			}
			else if (secondChoice == 2)
			{
				//float functions
				bool floatFunction = choosefloatArrayFunctions(floatArray);
				if (floatFunction == true)
				{
					chooseArrayDatatype(intArray, floatArray, stringArray);
				}
			}
			else if (secondChoice == 3)
			{
				//string functions
				bool stringFunction = chooseStringArrayFunctions(stringArray);
				if (stringFunction == true)
				{
					chooseArrayDatatype(intArray, floatArray, stringArray);
				}
			}
			else if (secondChoice == 4)
			{
				exit=true;
			}
			else
			{
				cout << "Invalid Input. Please enter 1-3 only." << endl;
				cin.ignore();
			}

			//clears the buffer
			cin.clear();
		} while (exit == false);
	return exit;

}

//Integer functions , push, 
bool chooseIntArrayFunctions(OrderedArray<int>& intArray)
{
	Timer myTimer;

	lint time = myTimer.GetMS();

	bool exit = false;
	do{
	showArrayFunctions();

	int thirdChoice = 0;
	cin >> thirdChoice;
	if (!cin)
	{
		cin.clear();
		cin.ignore(80, '\n');
	}
	else
	{


		if (thirdChoice == 1)
		{

			cout << "Current Array size is: " << intArray.Size() << endl;
			//cout << "Enter the new size of array." << endl;

			int newSize = checkIntInput("Enter the new size of array");
			if (newSize != 99999999)
			{
				intArray.resize(newSize);
			}
		}

		else if (thirdChoice == 2)
		{
			int newSize = checkIntInput("Enter an integer for grow size everytime when array is full");
			if (newSize != 99999999)
			{
				intArray.setGrowSize(newSize);
			}
			
			
		}
		else if (thirdChoice == 3)
		{
			int push = checkIntInput("Enter an integer to add to array.");
			if (push != 99999999)
			{
				intArray.push(push);
			}
			
		}
		else if (thirdChoice == 4)
		{
			int remove= checkIntInput("Enter index you wish to remove from array.");
			if (remove != 99999999)
			{
				intArray.Remove(remove);
			}
		}
		else if (thirdChoice == 5)
		{
			intArray.pop();
		}
		else if (thirdChoice == 6)
		{
			int search = checkIntInput("Enter integer you wish to search index.");
			if (search != 99999999)
			{
				

				myTimer.Reset();
				int index=intArray.binarySearch(search);
				cout << "Index of " << search << ":" << index << endl;
				cout << "The time(Milliseconds) used :" <<time << endl;
			}
		}
		else if (thirdChoice == 7)
		{
			int search = checkIntInput("Enter integer you wish to search index.");
			if (search != 99999999)
			{
				myTimer.Reset();
				int index=intArray.linearSearch(search);
				cout << "Index of " << search << ":" << index << endl;
				cout << "The time(Milliseconds) used :" << time << endl;
			}
		}
		else if (thirdChoice == 8)
		{
			intArray.print();
		}
		else if (thirdChoice == 9)
		{
			intArray.clear();
		}
		else if (thirdChoice == 10)
		{
			cout << "Enter filename to load from." << endl;
			string filename;
			cin >> filename;
			intArray.ReadUnorderedFile(filename);
		}
		else if (thirdChoice == 11)
		{
			cout << "Enter filename to save to." << endl;
			string filename;
			cin >> filename;
			intArray.WriteOrderedFile(filename);
		}
		else if (thirdChoice == 12)
		{
			exit = true;
		}
		else
		{
			cout << "Choice is invalid. Please enter number 1-12 only.";

		}
	cin.ignore(2);
	}
}while (exit == false);

	
	return exit;
}


bool choosefloatArrayFunctions(OrderedArray<float>& floatArray)
{
	Timer myTimer;

	lint time = myTimer.GetMS();
	bool exit = false;
	do{
		showArrayFunctions();

		int thirdChoice = 0;
		cin >> thirdChoice;
		if (!cin)
		{
			cin.clear();
			cin.ignore(100, '\n');
		}
		else
		{


			if (thirdChoice == 1)
			{

				cout << "Current Array size is: " << floatArray.Size() << endl;
				//cout << "Enter the new size of array." << endl;

				int newSize = checkIntInput("Enter the new size of array");
				if (newSize != 99999999)
				{
					floatArray.resize(newSize);
				}
			}

			else if (thirdChoice == 2)
			{
				int newSize = checkIntInput("Enter an integer for grow size everytime when array is full");
				if (newSize != 99999999)
				{
					floatArray.setGrowSize(newSize);
				}


			}
			else if (thirdChoice == 3)
			{
				float push = checkFloatInput("Enter a float to add to array.");
				if (push != 999.9f)
				{
					floatArray.push(push);
				}

			}
			else if (thirdChoice == 4)
			{
				int remove = checkIntInput("Enter index of array you wish to remove.");
				if (remove != 999.9f)
				{
					floatArray.Remove(remove);
				}
			}
			else if (thirdChoice == 5)
			{
				floatArray.pop();
			}
			else if (thirdChoice == 6)
			{
				float search = checkFloatInput("Enter float you wish to search index.");
				if (search != 999.9f)
				{
					myTimer.Reset(); 
					int index=floatArray.binarySearch(search);
					cout << "Index of " << search << ":" << index << endl;
					cout << "The time(Milliseconds) used :" << time << endl;
				}
			}
			else if (thirdChoice == 7)
			{
				float search = checkFloatInput("Enter float you wish to search index.");
				if (search != 999.9f)
				{
					myTimer.Reset(); 
					int index=floatArray.linearSearch(search);
					cout << "Index of " << search << ":" << index << endl;
					cout << "The time(Milliseconds) used :" << time << endl;
				}
			}
			else if (thirdChoice == 8)
			{
				floatArray.print();
			}
			else if (thirdChoice == 9)
			{
				floatArray.clear();
			}
			else if (thirdChoice == 10)
			{
				cout << "Enter filename to load from." << endl;
				string filename;
				cin >> filename;
				floatArray.ReadUnorderedFile(filename);
			}
			else if (thirdChoice == 11)
			{
				cout << "Enter filename to save to." << endl;
				string filename;
				cin >> filename;
				floatArray.WriteOrderedFile(filename);
			}
			else if (thirdChoice == 12)
			{
				exit = true;
			}
			else
			{
				cout << "Choice is invalid. Please enter number 1-12 only.";
				
			}cin.ignore(2);
		}
	} while (exit == false);


	return exit;
}

bool chooseStringArrayFunctions(OrderedArray<std::string>& stringArray)
{
	Timer myTimer;
	lint time = myTimer.GetMS();

	bool exit = false;
	do{
		showArrayFunctions();

		int thirdChoice = 0;
		cin >> thirdChoice;
		if (!cin)
		{
			cin.clear();
			cin.ignore(100, '\n');
		}
		else
		{


			if (thirdChoice == 1)
			{
				cout << "Current Array size is: " << stringArray.Size() << endl;
				//cout << "Enter the new size of array." << endl;

				int newSize = checkIntInput("Enter the new size of array");
				if (newSize != 99999999)
				{
					stringArray.resize(newSize);
				}
			}

			else if (thirdChoice == 2)
			{
				int newSize = checkIntInput("Enter an integer for grow size everytime when array is full");
				if (newSize != 99999999)
				{
					stringArray.setGrowSize(newSize);
				}
			}
			else if (thirdChoice == 3)
			{
				cout << "Enter string you wish to add." << endl;
				string word;
				cin >> word;
				stringArray.push(word);

			}
			else if (thirdChoice == 4)
			{
				cout << "Enter index of string array you wish to remove." << endl;
				int index;
				cin >> index;
				stringArray.Remove(index);				
			}
			else if (thirdChoice == 5)
			{
				stringArray.pop();
			}
			else if (thirdChoice == 6)
			{
				cout << "Enter string you wish to search index." << endl;
				string word;
				cin >> word;
				myTimer.Reset(); 
				int index=stringArray.binarySearch(word);
				cout << "Index of " << word << ":" << index << endl;
				cout << "The time(Milliseconds) used :" << time << endl;
			}
			else if (thirdChoice == 7)
			{
				
				cout << "Enter string you wish to search index." << endl;
				string word;
				cin >> word;
				myTimer.Reset(); 
				int index=stringArray.linearSearch(word);
				cout << "Index of " << word << ":" << index << endl;
				cout << "The time(Milliseconds) used :" << time << endl;
			}
			else if (thirdChoice == 8)
			{
				stringArray.print();
			}
			else if (thirdChoice == 9)
			{
				stringArray.clearString();
			}
			else if (thirdChoice == 10)
			{
				cout << "Enter filename to load from." << endl;
				string filename;
				cin >> filename;
				stringArray.ReadUnorderedFile(filename);
			}
			else if (thirdChoice == 11)
			{
				cout << "Enter filename to save to." << endl;
				string filename;
				cin >> filename;
				stringArray.WriteOrderedFile(filename);
			}
			else if (thirdChoice == 12)
			{
				exit = true;
			}
			else
			{
				cout << "Choice is invalid. Please enter number 1-12 only.";
			}cin.ignore(2);
		}
	} while (exit == false);


	return exit;
}

void showArrayFunctions()
{
	system("CLS");
	cout << "1. Change array size" << endl;
	cout << "2. Change array grow size" << endl;
	cout << "3. Push element into array" << endl;
	cout << "4. Remove element from array" << endl;
	cout << "5. Pop last element from array" << endl;
	cout << "6. Binary search index" << endl;
	cout << "7. Linear search index" << endl;
	cout << "8. Print all elements in array" << endl;
	cout << "9. Clear array" << endl;
	cout << "10. Load array from text file" << endl;
	cout << "11. Save array to text file" << endl;
	cout << "12. back" << endl;
}

int checkIntInput(string toPrint)
{
	cout << toPrint << endl;
	int number;
	cin >> number;
	if (!cin)
	{
		//clear the buffer
		cin.clear();
		//ignore the line of 100 characters till its a new line
		cin.ignore(100, '\n');
		cout << "Invalid Input. Please enter a valid INTEGER." << endl;
		return 99999999;
	}
	else
	{
		return number;
		cin.clear();
	}
}

float checkFloatInput(string toPrint)
{
	cout << toPrint << endl;
	float number;
	
	cin >> number;
	if (!cin)
	{
		//clear the buffer
		cin.clear();
		//ignore the line of 100 characters till its a new line
		cin.ignore(100, '\n');
		cout << "Invalid Input. Please enter a valid INTEGER." << endl;
		return 999.9f;
	}
	else
	{
		return number;
	}
}


