#ifndef	MENU_H
#define MENU_H

#include<string>
#include<iostream>
#include"OrderedArray.h"

bool showMainMenu(OrderedArray<int>& intArray, OrderedArray<float>& floatArray, OrderedArray<std::string>& stringArray);
bool chooseArrayDatatype(OrderedArray<int>& intArray, OrderedArray<float>& floatArray, OrderedArray<std::string>& stringArray);
bool chooseIntArrayFunctions(OrderedArray<int>& intArray);
bool choosefloatArrayFunctions(OrderedArray<float>& floatArray);
bool chooseStringArrayFunctions(OrderedArray<std::string>& stringArray);
void showArrayFunctions();
int checkIntInput(std::string toPrint);
float checkFloatInput(std::string toPrint);
#endif