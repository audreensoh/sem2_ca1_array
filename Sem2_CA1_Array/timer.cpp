//implement timer.

#include "timer.h"
#include <ctime>
#ifdef _WIN32
#include <Windows.h>
#else
#include <sys/time.h>
#endif

#ifdef _WIN32
class Win32PerformanceCounter
{
public:
	lint m_freq;
	Win32PerformanceCounter()
	{
		//get the 'tick per second'
		QueryPerformanceFrequency((LARGE_INTEGER*)(&m_freq));
		m_freq = m_freq / 1000;
	}
};
Win32PerformanceCounter g_Win32Counter;
#endif

//===========================================================================================================
//These function get a time value.Meaning of time is relative.
//===========================================================================================================

lint GetTimeMS()
{
#ifdef _WIN32
	lint t;
	QueryPerformanceCounter((LARGE_INTEGER*)(&t));
	return t / g_Win32Counter.m_freq;
#else
	struct timeval t;
	lint s;
	gettimeofday(&t, 0);
	s = t.tv_sec;
	s *= 1000;
	s += (t.tv_usec / 1000);

	return s;
#endif
}

lint GetTimeS()
{
	return GetTimeMS() / 1000;
}

lint GetTimeM()
{
	return GetTimeMS() / 60000;
}


lint GetTimeH()
{
	return GetTimeMS() / 3600000;
}

std::string TimeStamp()
{
	char str[9];

	time_t a = time(0);
	struct tm b;
	localtime_s(&b, &a);
	//where is this function from?!
	strftime(str, 9, "%H:%M:%S", &b);
	return str;
}

//this print datastamp in YYYY:MM:DD

std::string DateStamp()
{
	char str[11];

	time_t a = time(0);
	struct tm b;
	localtime_s(&b, &a);
	strftime(str, 11, "%Y:%m:%d", &b);
	return str;
}

//constructor uses intializer list
//Always try to do this because it means that setting values is a one step process rather than 2
Timer::Timer() :m_startTime(0), m_initTime(0)
{

}

void Timer::Reset(lint p_timepassed)
{
	m_startTime = p_timepassed;
	m_initTime = GetTimeMS();
}

lint Timer::GetMS()
{
	//return the amount of time that has passed since the rime was initialized, plus whatever starting time the timer had
	return (GetTimeMS() - m_initTime) + m_startTime;
}

lint Timer::GetTS()
{
	return GetMS() / 1000;
}

lint Timer::GetM()
{
	return GetMS() / (1000 * 60);
}

lint Timer::GetH()
{
	return GetMS() / (1000 * 60 * 60);
}

lint Timer::GetD()
{
	return GetMS() / (1000 * 60 * 60 * 24);
}

lint Timer::GetY()
{
	return GetMS() / (1000 * 60 * 60 * 24 * 365);
}

std::string Timer::GetString()
{
	std::string str;
	lint y = GetY();
	lint d = GetD() % 365;
	lint h = GetH() % 24;
	lint m = GetM() % 60;

	if (y > 0)
	{
		str += y + "Years, ";
	}
	if (d > 0)
	{
		str += d + "days, ";
	}
	if (h > 0)
	{
		str += h + "hours, ";
	}
	str += m + "minutes";

	return str;


}